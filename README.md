# pystan-workshop

This is the material for a workshop on Bayesian sampling in Pystan held by Lea Helmers on the 23rd of Nov 2018

* To use the material, clone the repository!

* For setting up, make sure you have ``pipenv`` installed
* Then, ``cd`` into the local copy of the repository and run

``pipenv install``

To see the jupyter slides as HTML in your browser, run

``jupyter nbconvert workshop_slides.ipynb --to slides --post serve``

Have fun :)

